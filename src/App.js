import logo from "./logo.svg";
import "./App.css";

//importamos el componente creado
//import Componente1 from './componentes/Componente1';

function App() {
  let nombre = "Valentin Pico";
  let presentacion = <h1>hola soy, {nombre}</h1>;

  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Edit <code>src/App.js</code> and save to reload.
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
      </header>
    </div>
  );
}

/* function holaMundo(nombre) {
  let presentacion = <h1>hola soy, {nombre}</h1>;

  return presentacion;
}
 */
export default App;
